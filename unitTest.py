# Task 2
import unittest
from linearSearch import linearSearch
from binarySearch import binarySearch

class SearchTestCase(unittest.TestCase):
	
# Test cases for linear search
	def test_linearSearch(self):
		values = [1,3,6,9,11,12,14,17,19]
		self.assertEqual(linearSearch(values, 1), 0)
		self.assertEqual(linearSearch(values, 9), 3)
		self.assertEqual(linearSearch(values, 19), 8)
		self.assertEqual(linearSearch(values, 13), -1)
# Test cases for binary search
	def test_binarySearch(self):
		values = [1,3,6,9,11,12,14,17,19]
		self.assertEqual(binarySearch(values, 1), 0)
		self.assertEqual(binarySearch(values, 9), 3)
		self.assertEqual(binarySearch(values, 19), 8)
		self.assertEqual(binarySearch(values, 13), -1)
if __name__ == '__main__':
	unittest.main()
