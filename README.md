COMP 314 Lab work 1

Task 1: Implement linear and binary search algorithms.

Binary Search is implemented in binarySearch.py and Linear Search is implemented in linearSearch.py.

Task 2: Write some test cases to test your program.

Some test cases to test binary search and linear search programs are written in unitTest.py

Task 3: Generate  some  random  inputs  for  your  program  and  apply  both  linearand binary search algorithms to find a particular element on the generatedinput.  Record the execution times of both algorithms for best and worst cases on inputs of different size (e.g.  from 10000 to 100000 with step sizeas 10000).  Plot an input-size vs execution-time graph.

This is done in task3.py and the output graph is shown in Graph.png.


Task 4: Explain your observations.

From the graph of best case and worst case of linear and binary search algorithms for input size 100000 to 1000000 with step size 100000, we see that In the worst case scenario Binary Search is much more faster than Linear Search. While in the best case scenario both algorithms have similarly fast runtimes.