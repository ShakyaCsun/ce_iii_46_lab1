def binarySearch(list, target):
	list.sort()
	min = 0
	max = len(list) - 1
	found = -1
	while min <= max:
		mid = (min + max) // 2		
		if list[mid] == target:
			found = mid
			break
		else:
			if target < list[mid]:
				max = mid - 1
			else:
				min = mid + 1
	return found

if __name__ == '__main__':
	List = [1,3,6,9,11,12,14,17,19]
	List.sort()
	print("Element in List :", List)
	x = int(input("Enter element to search:"))

	print("Performing Binary Search")
	result = binarySearch(List, x)
	
	if result==-1:
		print("Element not found in the list")
	else:
		print("Element " + str(x) + " is found at position %d" %(result))
