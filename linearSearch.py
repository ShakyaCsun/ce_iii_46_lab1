def linearSearch(list, target):
	list.sort()
	found = -1
	for i in range(len(list)):
		if list[i] == target:
			found = i
			break
	return found

if __name__ == '__main__':
	List = [1,3,6,9,11,12,14,17,19]
	print("Element in List :", List)
	x = int(input("Enter element to search:"))

	print("Performing Linear Search")
	result = linearSearch(List, x)
	if result == -1:
		print("Element not found in the list")
	else:
		print("Element " + str(x) + " is found at position %d" %(result))
