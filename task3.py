from linearSearch import linearSearch
from binarySearch import binarySearch
import random
from time import time
import matplotlib.pyplot as plt

n = 100000
i = 0
linearSearchTimes = []
binarySearchTimes = []
sizeArray = []

print('Worst Case Scenarios')
for i in range(n, n * 11, n):
	sizeArray.append(i)
	randomValues = random.sample(range(i), i)
	#time taken in linear search
	startTime = time()
	linearSearch(randomValues, i) # Worst case for linear search is if target item is at last index or can't find it
	endTime = time()
	timeLinear = endTime - startTime
	linearSearchTimes.append(timeLinear)
	print(timeLinear, "s taken by Linear Search for size", i)
	#time taken in binary search
	startTime = time()
	index = binarySearch(randomValues, i) # Worst case for binary search is if target item is at last or first index or can't find it
	endTime = time()
	timeBinary = endTime - startTime
	binarySearchTimes.append(timeBinary)
	print(timeBinary, "s taken by Binary Search for size", i)


	

# Graph for time vs size for worst case of both algorithms
plt.figure(1)
plt.plot(sizeArray,linearSearchTimes, label = 'Linear Search')
plt.plot(sizeArray,binarySearchTimes, label = 'Binary Search')
plt.legend(loc = 'upper center', shadow = True, fontsize = 'large')
plt.xlabel('Size')
plt.ylabel('Time in seconds')
plt.title('Worst Case')

print('Best Case Scenarios')
linearSearchTimes = []
binarySearchTimes = []
sizeArray = []

for i in range(n, n * 11, n):
	sizeArray.append(i)
	randomValues = random.sample(range(i), i)
	#time taken in linear search
	startTime = time()
	linearSearch(randomValues, 0) # Best case for linear search is if target item is at first index
	endTime = time()
	timeLinear = endTime - startTime
	linearSearchTimes.append(timeLinear)
	print(timeLinear, "s taken by Linear Search for size", i)
	#time taken in binary search
	startTime = time()
	binarySearch(randomValues, (i-1)//2) # Best case for binary search is if target item is at mid index
	endTime = time()
	timeBinary = endTime - startTime
	binarySearchTimes.append(timeBinary)
	print(timeBinary, "s taken by Binary Search for size", i)


# Graph for time vs size for best case of both algorithms
plt.figure(2)
plt.plot(sizeArray,linearSearchTimes, label = 'Linear Search')
plt.plot(sizeArray,binarySearchTimes, label = 'Binary Search')
plt.legend(loc = 'upper center', shadow = True, fontsize = 'large')
plt.xlabel('Size')
plt.ylabel('Time in seconds')
plt.title('Best Case')
plt.show()
